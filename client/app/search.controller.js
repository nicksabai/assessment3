// Always use an IIFE, i.e., (function() {})();
(function () {
    // Attaches a SearchCtrl to the Fitly module
    angular
        .module("fitly")
        .controller("SearchCtrl", SearchCtrl);

    // Inject the custom UserService service so your controller can use its functionalities
    // Dependency injection. Here we inject UserService so SearchCtrl can call services related to employee.
    SearchCtrl.$inject = ['UserService'];


    // Accept the injected dependency as a parameter.
    // Search function declaration
    function SearchCtrl(UserService) {

        // Declares the ViewModel and assigns it the object this (in this case, the SearchCtrl). Any
        // function or variable that you attach to vm will be exposed to callers of SearchCtrl, e.g., search.html
        var searchCtrl = this;

        // Exposed data models -----------------------------------------------------------------------------------------
        // Change users to an empty array. Move current contents to server.
        searchCtrl.users = '';

        // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view. Currently, search.controller.js doesn't have any exposed
        // functions

        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        // Call init. Init is a private function
        // init is a private function (i.e., not exposed)
        init();

        // Function declaration and definition -------------------------------------------------------------------------
        // Define init() function. This function would call appropriate UserService service to populate
        // View with initial value
        // The init function initializes view
        function init() {

            // In init function, populate search.html with users data from server
            // 13.3 Handle promise object using .then() and .catch()
            // On success, fill searchCtrl.users with data from server
            // We call UserService.retrieveUser to handle retrieval of user information. The data retrieved from
            // this function is used to populate search.html.
            UserService
                .retrieveUsers()
                .then(function(results){
                    searchCtrl.users = results.data;
                })
                .catch(function(err){
                    console.log("error " + err);
                });
        }

    }
})();