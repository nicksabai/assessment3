(function() {
    var app = angular.module('fitly', ['ui.router']);

    app.config(FitlyConfig);
    app.service("UserSvc", UserSvc);  
    app.controller("LoginCtrl", LoginCtrl);
    app.controller("ListCtrl", ListCtrl);

    // ===================================================================================
    // FitlyConfig to set up various states of the system
    // ===================================================================================
    FitlyConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
    function FitlyConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'views/login.html',
                controller: 'LoginCtrl as loginCtrl'
            })
            .state('list', {
                url: '/list',
                templateUrl: 'views/list.html',
                controller: 'ListCtrl as listCtrl'
            })
            .state('add', {
                url:'/add',
                templateUrl: 'views/add.html',
                controller: 'AddCtrl as addCtrl'
            })
            .state('view', {
                url:'/view/:userId',
                templateUrl: 'views/view.html',
                controller: 'ViewCtrl as viewCtrl'
            })
            .state('edit', {
                url:'/edit/:userId',
                templateUrl: 'views/edit.html',
                controller: 'EditCtrl as editCtrl'
            });
            $urlRouterProvider.otherwise('/login');
    };

    // ===================================================================================
    // UserSvc to provide central user-database-related services
    // ===================================================================================
    UserSvc.$inject = ['$http'];    // Inject $http to use built-in service to communicate with server
    function UserSvc($http) {       // UserService function declaration

        // Declare 'service' and assigns it the object 'this'
        // Any function or variable attached to userService will be exposed to callers of
        // UserService, e.g. search.controller.js and register.controller.js
        var userSvc = this;

        // EXPOSED DATA MODELS -------------------------------------------------------------------------------------
        // EXPOSED FUNCTIONS -------------------------------------------------------------------------------------
        userSvc.insertUser = insertUser;
        userSvc.retrieveUserById = retrieveUserById;
        userSvc.updateUser = updateUser;
        userSvc.deleteUser = deleteUser;
        userSvc.retrieveUsers = retrieveUsers;     
        userSvc.loginUser = loginUser;
        userSvc.logoutUser = logoutUser;

        // FUNCTION DECLARATION AND DEFINITION -------------------------------------------------------------------------
        // insertUser uses HTTP POST to send user information to the server's /users route
        // Parameters: user information; Returns: Promise object
        function insertUser(newUser) {
            // This line returns the $http to the calling function
            // This configuration specifies that $http must send the user data received from the calling function
            // to the /users route using the HTTP POST method. $http returns a promise object. In this instance
            // the promise object is returned to the calling function
            return $http({
                method: 'POST',
                url: 'api/users',
                data: {user: newUser}
            });
        };
        
        // retrieveUserById retrieves specific User information from the server via HTTP GET.
        // Parameters: userId. Returns: Promise object
        function retrieveUserById(userId) {
            return $http({
                method: 'GET',
                url: 'api/users/' + userId
            });
        };

        // updateUser updates a specific User information from the server via HTTP GET.
        // Parameters: user object. Returns: Promise object
        function updateUser(updatedUser) {
            return $http({
                method: 'PUT',
                url: 'api/user/' + updateUser.userId,
                data: {
                    user: updateUser
                }
            });
        };

        // deleteUser deletes a user by userId
        // Parameters: userId. Returns: Promise object!
        function deleteUser(userId) {
            return $http({
                method: 'DELETE',
                url: 'api/users/' + userId
            });
        };

        // retrieveUsers retrieves user information from the server via HTTP GET
        // Passes information via the query string (params)
        // Parameters: searchString. Returns: Promise object
        function retrieveUsers(searchString){
            return $http({
                method: 'GET',
                url: 'api/users',
                params: {
                    'keyword': searchString
                }
            });
        };
        
        // loginUser authenticates the user against server details
        // Parameters: userProfile (email & password). Returns: Promise object!
        function loginUser(userProfile) {
            return $http({
                method: 'POST',
                url: '/api/users/auth',
                data: {user: userProfile}
            });
        };

        // logoutUser logouts user out of the current session
        // Parameters: none. Returns: Promise object!
        function logoutUser() {
            return $http({
                method: 'GET',
                url: '/api/users/signout'
            });
        };

    }; // end of UserSvc

    // ===================================================================================
    // LoginCtrl to handle the initial user login
    // ===================================================================================
    LoginCtrl.$inject = ['$state'];
    function LoginCtrl($state) {
        loginCtrl = this;

        loginCtrl.login = function() {
            // Call UserSvc.loginUser to initiate authentication
            // Hack to go straight to dashboard
            $state.go('list');
            
        };
    };

    // ===================================================================================
    // ListCtrl to handle the central display of users
    // ===================================================================================
    ListCtrl.$inject = ['$state', 'UserSvc'];
    function ListCtrl($state, UserSvc) {
        listCtrl = this;

        listCtrl.searchString = "";
        listCtrl.users = {};

        // display initial list of users by default
        getList();

        function getList() {
            console.log("value of searchString", listCtrl.searchString);
            UserSvc.retrieveUsers(listCtrl.searchString)
                .then(function(users){
                    console.log("users returned: ", users);
                    listCtrl.users = users.data;
                }).catch(function(err){
                    console.error("Error encountered: ", err);
                });
        };

        listCtrl.getList = getList();

        listCtrl.addUser = function(){
            $state.go("add");
        };

        listCtrl.viewUser = function(){
            $state.go("view");
        };

        listCtrl.editUser = function(userIdToEdit){
            $state.go("edit", {'userId' : userIdToEdit});
        };

        listCtrl.deleteUser = function(){
            // have a simple pop-up window to confirm
            // if yes, call UserSvc.deleteUser()
        };
    };

    // ===================================================================================
    // AddCtrl to handle addition of a new user
    // ===================================================================================
    AddCtrl.$inject = ['$state', 'UserSvc'];
    function AddCtrl($state, UserSvc) {
        addCtrl = this;

        addCtrl.user = {};
        // Have a form to collect these fields and then call UserSvc.insertUser to save to db
        // email
        // firstname
        // lastname
        // password
        // qualifications - checkboxes of: Crossfit, MMA, Core, Weights, Spinning, Aerobics, Boxing, Barre
        // set role to 1 for 'trainer'
        // set status to 0 for 'active'
    };

    // ===================================================================================
    // ViewCtrl to handle viewing of a user
    // ===================================================================================
    ViewCtrl.$inject = ['$state', '$stateParams', 'UserSvc'];
    function ViewCtrl($state, $stateParams, UserSvc) {
        viewCtrl = this;

        idOfUserToView = $stateParams.userId;
        // call UserSvc.retrieveUserById by passing idOfUserToView
    };

    // ===================================================================================
    // EditCtrl to handle editing of a new user
    // ===================================================================================
    EditCtrl.$inject = ['$state', '$stateParams', 'UserSvc'];
    function EditCtrl($state, $stateParams, UserSvc) {
        editCtrl = this;
        
        idOfUserToEdit = $stateParams.userId;
        editCtrl.user = {};

        // call UserSvc.retrieveUserById by passing idOfUserToEdit
        // pass returned record to editCtrl.user to automatically populate fields

        // then call UserSvc.updateUser to save changes back to server
    };

})();
