// model definition of 'schedules' table in 'fitly' database
module.exports = function(database, Sequelize) {
    // 'schedules' is the same name as the SQL database
    var Schedule = database.define('schedules', {
        // map everything here just like it is on SQL 'schedules'
            sid: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            kid: {
                type: Sequelize.INTEGER(11),
                allowNull: false
            },
            uid: {
                type: Sequelize.INTEGER(11),
                allowNull: false
            },
            datastart: {
                type: Sequelize.DATE,
                allowNull: false
            },
            duration: {
                type: Sequelize.INTEGER(11),
                allowNull: false
            },
            minsize: {
                type: Sequelize.INTEGER(11),
                allowNull: false
            },
            maxsize: {
                type: Sequelize.INTEGER(11),
                allowNull: false
            },
            instructions: {
                type: Sequelize.STRING(255),
                allowNull: false,
                charset: 'utf8',
                collate: 'utf8_unicode_ci'
            },
            status: {
                type: Sequelize.BOOLEAN,
                allowNull: false
            },
            createdAt: {
                type: Sequelize.DATE,
                allowNull: false
            },
            updatedAt: {
                type: Sequelize.DATE,
                allowNull: false
            },
            deletedAt: {
                type: Sequelize.DATE,
                allowNull: false
            }
        }, {
            // 'schedules' is the table name on the 'fitly' database
            tableName: 'schedules',
            // Allow timestamp attributes (updatedAt, createdAt)
            // By default, added to know when db entry added & last updated
            timestamps: true,
            paranoid: true
        }
    );
    return Schedule;
};